<?php
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--stylesheet and js file from bootstrap and fontawesome-->
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="resources/font-awesome/css/font-awesome.min.css">
    <link href="resources/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

    <!-- Owl-carousel -->
    <link rel="stylesheet" href="resources/OwlCarousel/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="resources/OwlCarousel/dist/assets/owl.theme.default.min.css">

    <!-- stylesheet and js file from local -->
    <link rel="stylesheet" href="resources/css/style.css?v-1">
    <link rel="shortcut icon" href="assets/favicon/favicon.ico" type="image/x-icon">
    <title>Chans Cold Coconuts</title>
</head>

<body>
    <!-- header -->
    <section id="header">
        <div class="container-fluid top-titlebar">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-4 offset-lg-1">
                        <ul class="top-bar-item-01" style="display: flex; padding: 35px; margin-left: 148px;">
                            <li>
                                <a href=" #">
                                    <span class="header-icon-01"> <i class="fa fa-location-arrow"></i></span>
                                    <span class="header-txt-01"> Kissimmee, Florida</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" style="margin-left: 15px;">
                                    <span class="header-icon-01"><i class="fa fa-phone"></i></span>
                                    <span class="header-txt-02"> 917.981.5442</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-lg-offset-1">
                        <ul class="top-bar-item-02" style="display: flex; float: right; padding: 23px;">
                            <li>
                                <a href="#" target="_blank">
                                    <span class="fb-icon"><img src="assets/facebook.svg" alt=""></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="insta-icon"><img src="assets/instagram.svg" alt="instagram-icon"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="navbar">
        <div class="container-fluid main-menu">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-4 logo-text-wrapper">
                        <a href="#">
                            <h2>Chan's Cold Coconut</h2>
                        </a>
                    </div>
                    <div class="col-lg-8 collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home <span class="sr-only"></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Events</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- slider owl-carousel-->
    <section id="slide-bar">
        <div class="container-fluid">
            <div class="row">
                <!-- <div class="col-lg-12"> -->
                <div class="owl-carousel owl-theme owl-loaded">
                    <div class="owl-stage-outer">
                        <div class="owl-stage">
                            <div class="owl-item">
                                <div class="slide-img">
                                    <img src="assets/image/SlidShow1-scaled-1024x684-1.jpg" alt="SlidShow1">
                                </div>
                                <div class="slide-txt-01">
                                    <h2>Cold Coconut</h2>
                                    <ul>
                                        <li>No Preservatives</li>
                                        <li>Less Sodium</li>
                                        <li>Natural Electrolyte</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="owl-item">
                                <div class="slide-img">
                                    <img src="assets/image/SlidShow2-scaled-1024x909-1.jpg" alt="SlidShow1">
                                </div>
                                <div class="slide-txt-02">
                                    <h2>Cold Coconut</h2>
                                    <ul>
                                        <li>No added Sweetener</li>
                                        <li>Non-GMO</li>
                                        <li>Fat Free</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="owl-item">
                                <div class="slide-img">
                                    <img src="assets/image/SlidShow3-scaled-1024x681-1.jpg" alt="SlidShow1">
                                </div>
                                <div class="slide-txt-03">
                                    <h2>Cold Coconut</h2>
                                    <ul>
                                        <li>Anti-Oxidents</li>
                                        <li>Low Calories</li>
                                        <li>Helps Kidneys Bladder</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="owl-item">
                                <div class="slide-img">
                                    <img src="assets/image/SlidShow4-scaled-1024x681-1.jpg" alt="SlidShow1">
                                </div>
                                <div class="slide-txt-04">
                                    <h2>Cold Coconut</h2>
                                    <ul>
                                        <li>Anti-Inflammatory</li>
                                        <li>No Artificial Coloring</li>
                                        <li>Helps Weight Loss</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="owl-dots">
                        <div class="owl-dot active"><span></span></div>
                        <div class="owl-dot"><span></span></div>
                        <div class="owl-dot"><span></span></div>
                        <div class="owl-dot"><span></span></div>
                    </div>
                </div>
                <!-- </div> -->
            </div>
        </div>
    </section>

    <!-- advertisement widget 01 -->
    <section id="add-widget">
        <div class="container-fluid">
            <div class="row widget-title-01">
                <div class="col-lg-12 widget-header">
                    <h2>We are here today!</h2>
                    <p>We will be at the following Locations Today, See you there</p>
                </div>
            </div>
        </div>

    </section>

    <script src="resources/js/jquery.min.js"></script>

    <script src="resources/bootstrap/js/bootstrap.min.js"></script>

    <script src="resources/OwlCarousel/dist/owl.carousel.min.js"></script>

    <script src="resources/js/script.js"></script>



    <!-- javascripts for owl-carousal -->
    <script type="text/javascripts">
        $(document).ready(function() {
            $(".owl-carousel").owlCarousel();
        });
    </script>

</body>

</html>